Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: netdisco
Source: https://github.com/home-assistant-libs/netdisco

Files: *
Copyright: 2015-2018 Paulus Schoutsen <balloob@gmail.com>
           2015-2018 Home-Assistant developers
License: Apache-2.0

Files: CLA.md
Copyright: 2015-2018 Paulus Schoutsen <balloob@gmail.com>
           2015-2018 Home-Assistant developers
License: CC-BY-SA-3.0

Files: debian/*
Copyright: 2018 Ruben Undheim <ruben.undheim@gmail.com>
License: Apache-2.0


License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
   http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache-2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".


License: CC-BY-SA-3.0
 You are free to:
  - Share — copy and redistribute the material in any medium or format
  - Adapt — remix, transform, and build upon the material
  - for any purpose, even commercially.
 .
 The licensor cannot revoke these freedoms as long as you follow the license
 terms.
 .
 Under the following terms:
  - Attribution — You must give appropriate credit, provide a link to the
    license, and indicate if changes were made. You may do so in any reasonable
    manner, but not in any way that suggests the licensor endorses you or your
    use.
  - ShareAlike — If you remix, transform, or build upon the material, you must
    distribute your contributions under the same license as the original.
  - No additional restrictions — You may not apply legal terms or technological
    measures that legally restrict others from doing anything the license
    permits.
 .
 Notices:
  You do not have to comply with the license for elements of the material in
  the public domain or where your use is permitted by an applicable exception or
  limitation.  No warranties are given. The license may not give you all of the
  permissions necessary for your intended use. For example, other rights such as
  publicity, privacy, or moral rights may limit how you use the material.
