Source: python-netdisco
Section: python
Testsuite: autopkgtest-pkg-pybuild
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ruben Undheim <ruben.undheim@gmail.com>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all,
               python3-setuptools
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-netdisco
Vcs-Git: https://salsa.debian.org/python-team/packages/python-netdisco.git
Homepage: https://github.com/home-assistant-libs/netdisco
Rules-Requires-Root: no


Package: python3-netdisco
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Description: Library to discover local devices and services (Python 3)
 This is a Python 3 library to discover local devices and services on the
 network. It allows one to scan on demand or offer a service that will scan the
 network in the background in a set interval.
 .
 Current methods of scanning:
 .
  - mDNS (includes Chromecast, Homekit)
  - uPnP
  - Plex Media Server using Good Day Mate protocol
  - Logitech Media Server discovery protocol
  - Daikin discovery protocol
  - Web OS discovery protocol
